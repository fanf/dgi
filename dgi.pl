#!/usr/bin/perl
#
# Written by Tony Finch <dot@dotat.at> <fanf@isc.org>
#
# Permission is hereby granted to use, copy, modify, and/or
# distribute this software for any purpose with or without fee.
#
# This software is provided 'as is', without warranty of any kind.
# In no event shall the authors be liable for any damages arising
# from the use of this software.
#
# SPDX-License-Identifier: 0BSD OR MIT-0

use warnings;
use strict;

my $query = $ENV{QUERY_STRING} // "";
$query =~ s{dig=}{};

my @query = grep m{.}, split m{[\s+]+}, $query;
@query = qw{-h} unless @query;

# URL unescape, needed for @ at least
grep s{(%([0-9A-Fa-f]{2}))}{chr hex $2}eg, @query;

my $SERVER = '@9.9.9.9';

my $byte = qr{( [0-9]
	      | [1-9][0-9]
	      | 1[0-9][0-9]
	      | 2[0-4][0-9]
	      | 25[0-5]
	      )}x;

# this is not a thorough blacklist
sub no1918 {
	my $server = $1;
	if ($server =~ m{^[@]( 192\.168\.
			 | 172\.1[6789]\.
			 | 172\.2\d\.
			 | 172\.3[01]\.
			 | 10\.
			 | 127\.
			 )}x) {
		return $SERVER;
	}
	if ($server =~ m{^[@]$byte\.$byte\.$byte\.$byte$}) {
		return $server;
	}
	# only IANA-allocated v6 unicast space
	if ($server =~ m{^[@] [23] [0-9A-Fa-f]{3}
			 : [0-9A-Fa-f:]+
			 $}x) {
		return $SERVER;
	}
	# no domain names, a mildly rude restriction
	return $SERVER;
}

my $haveserver = grep s{^(@.*)}{no1918}e, @query;

print <<RUBRIC;
X-Clacks-Overhead: GNU Terry Pratchett
Content-Security-Policy: base-uri 'self'; default-src 'none'; form-action https://dotat.at/cgi/dig; frame-ancestors 'none'; img-src 'self' https://www.isc.org/images/; style-src 'unsafe-inline';
X-Frame-Options: deny
X-XSS-Protection: 1; mode=block
Content-type: text/html

<!DOCTYPE html>

<style>
  body {
    margin: 1rem;
  }

  pre {
    margin: 0em;
  }

  form {
    color: white;
    background: #0c1874;
    padding: 1.5rem;
    display: flex;
  }

  a {
    flex-grow: 0;
    margin-right: 1rem;
  }

  .button {
    letter-spacing: 2px;
    margin-right: 1rem;
    flex-grow: 0;
  }

  .args {
    flex-grow: 1;
  }

  img, input {
    vertical-align: middle;
  }

  input {
    padding: 0.5rem;
    font-size: 1.5rem;
    color: inherit;
    background: #077fc3;
    border-radius: 0;
    border-style: solid;
    border-color: #ccc;
  }

</style>

<form action="https://dotat.at/cgi/dig">
  <a href="https://www.isc.org/bind/" tabindex="1">
    <img src="https://www.isc.org/images/logo.png"
         width="264" height="65" alt="isc.org">
  </a>
  <input type="submit" tabindex="2" class="button" value="DiG">
  <input type="text" tabindex="3" class="args"
         name="dig" value="@query" size="1"
         autofocus="true" autocorrect="off" autocapitalize="none">
</form>

<pre>
RUBRIC

print "\n" if "@query" eq "-h";

unshift @query, $SERVER unless $haveserver;

open STDERR, ">&STDOUT";
exec '/usr/bin/dig', @query;

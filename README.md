dgi: a CGI web front end to ISC BIND9 `dig`
===========================================

A basic form with a little styling that executes `dig`.

security
--------

  * I assume that `dig` can safely handle untrusted input on its
    command line.

  * I restrict the `@server` option to avoid sending queries to
    private IP addresses.

  * I do not run `dig` via a shell; the only pre-processing is to
    split command line arguments on spaces, and URL un-escaping.

  * I rely on HTTP security headers (Content-Security-Policy etc.)
    to protect against cross-site scripting, because I don't
    sanitize the output from `dig`.

licence
-------

Written by Tony Finch <<dot@dotat.at>> <<fanf@isc.org>>

Permission is hereby granted to use, copy, modify, and/or
distribute this software for any purpose with or without fee.

This software is provided 'as is', without warranty of any kind.
In no event shall the authors be liable for any damages arising
from the use of this software.

    SPDX-License-Identifier: 0BSD OR MIT-0

_[this is a zero-conditions libre software licence](https://dotat.at/0lib.html)_
